
# Installation

1. Clone the repo: `git clone git@gitlab.com:didehvar/typescript-koa-api.git`
2. Navigate into the directory: `cd typescript-koa-api`
3. Install npm packages: `yarn` or `npm i`

# Usage

Use `yarn start` or `npm start` to run.
