import * as Koa from 'koa';
import { config } from './config';
import middleware from './middleware';

export default () => {
  const app = new Koa();

  app.use(middleware);

  app.listen(config.port);

  return app;
};
