export const config: {
  port: string,
} = {
  port: process.env.PORT || '3000',
};
