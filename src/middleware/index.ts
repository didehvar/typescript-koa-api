import * as compose from 'koa-compose';
import { responseTime } from './response-time';

export default compose([
  responseTime,
]);
