import world from 'controllers/world';

it('returns world', () => {
  expect(world()).toBe('world');
});
