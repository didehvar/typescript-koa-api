jest.mock('server');

import 'index';
import server from 'server';

it('starts the server', () => {
  expect(server).toBeCalled();
});
