const useMock = jest.fn();
const listenMock = jest.fn();

jest.mock('koa', () => () => ({
  listen: listenMock,
  use: useMock,
}));

import { config } from 'config';
import { Context } from 'koa';
import server, { helloWorld } from 'server';

beforeEach(() => {
  server();
});

it('uses some middleware', () => {
  expect(useMock).toBeCalledWith(helloWorld);
});

it('listens on port env', () => {
  expect(listenMock).toBeCalledWith(config.port);
});

it('sets the body to hello world', () => {
  const ctx: Context = {} as Context;
  helloWorld(ctx);
  expect(ctx.body).toBe('hello world');
});
